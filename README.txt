Advanced Search Pages

This module allows site administrators to configure advanced search settings on any search page
created using the Search API Pages module. This module does not have any UI of its own but rather
extends Search API Pages and Search API Autocomplete to accomplish its goal.

To create a new advanced search, do the following:

1) Go to the Search API administration screen (admin/config/search/search_api) and create
	a search index. Visit the Fields page (admin/config/search/search_api/index/YOUR_INDEX/fields)
	and add any fields you would like to use for advanced searching. Note that if you would like
	the fields to appear as textfields with autocomplete functionality, they must be of type
	"Fulltext."

2) Go to the Search Pages administration screen (admin/config/search/search_api/page) and create
	the search page you would like to use for advanced searching. Save the page.

3) Edit the page you have just created. You will now see a fieldset called "Advanced Search Options."
	Open this fieldset and you will be able to select fields (but only those you have set to be
	indexed in step 1) to use for advanced searching. You can set the order you would like the chosen
	fields to appear on the Advanced Search form, as well as select whether or not users can choose
	multiple terms for each field, and set a custom field title and/or description for the Advanced
	Search form, if desired. When finished, save the search page.

4) Now, you may choose to enable autocomplete functionality on one or more of the Advanced Search
	fields you have created. Go back to the Search index page and you will see a tab for Autocomplete.
	The path to this page is admin/config/search/search_api/index/YOUR_INDEX/autocomplete).
	Under the fieldset "Advanced Search Fields," select which of the advanced search fields you would
	like to have autocomplete functionality. If one or more desired fields doesn't appear here,
	try going back to the Fields page of the search index admin form (step 1) and change the type
	of the field. Only fulltext fields can have autocomplete functionality.

5) Now, go to the path you have set for the search page you created in step 2. There should now
	be an "Advanced Search" fieldset that should show the fields you added. You can now perform
	searches using fields in this fieldset, with or without entering keywords in the original
	keyword textfield (which should be at the top).