(function ($) {

  /**
   * Puts the currently highlighted suggestion into the autocomplete field.
   */
  Drupal.jsAC.prototype.select = function (node) {
    if ($(this.input).hasClass('search-api-page-advanced')) {
      // Add whatever is already in the textfield to the suggestion, rather than replacing
      // whatever's in the textfield with the suggestion.
      var typed = this.input.value;
      var suggestion = $(node).data('autocompleteValue');
      //var codeStr = "this.input.value = '" + typed + suggestion + "';";
      var placeText = function(input, typed, suggestion) {
        $(input).val(typed + suggestion);
      };
      var input = this.input;
      setTimeout(function(){placeText(input, typed, suggestion); typed = null; suggestion = null;}, 10);
    }
    else{
        this.input.value = $(node).data('autocompleteValue');
    }
  };
  
})(jQuery);